@Echo off
cls
:: Save path
pushd .

::
:: Goto Infra-XS script directory
cd "C:\ProgramData\Geyer und Weinig\INFRA-XS\default\scripts" 

echo Agent         : [%1]
echo Flags         : [%2]
echo Step name     : [%3]
echo Logoff/login  : [%4]
echo Target script : [%5]
echo Portalsuffix  : [%6]

::
:: Start loadtest script and pass id parameter 
Echo Run Loadtest Skript
"C:\Program Files (x86)\Geyer und Weinig\INFRA-XS\bin\wxscr32.exe" runloadtest.gws %1 %2 %3 %4 %5 %6

goto END

:daten
:: copy /b/v/y "C:\ProgramData\Geyer und Weinig\INFRA-XS\default\export\*.csv" d:\batch\tasks\applications
:: wait for data to propagate
timeout /T 60 /nobreak >nul
"C:\Program Files (x86)\Geyer und Weinig\INFRA-XS\bin\7z.exe"  a d:\batch\tasks\applications\messdaten.7z "C:\ProgramData\Geyer und Weinig\INFRA-XS\default\olfmess\*.*"
"C:\Program Files (x86)\Geyer und Weinig\INFRA-XS\bin\7z.exe"  a d:\batch\tasks\applications\messdaten.7z "C:\ProgramData\Geyer und Weinig\INFRA-XS\default\export\*.csv"
"C:\Program Files (x86)\Geyer und Weinig\INFRA-XS\bin\7z.exe"  a d:\batch\tasks\applications\messdaten.7z "C:\ProgramData\Geyer und Weinig\INFRA-XS\default\logs\message.log"

:END
popd