@Echo off
::
::  Avoid script error not able to find chrome on first startup
cd "C:\Program Files (x86)\Google\Chrome\Application\"
Start "" /b chrome.exe
timeout /T 10 /nobreak >nul
taskkill /IM chrome.exe /F

echo Get GUID for this node
FOR /F %%a IN ('POWERSHELL -COMMAND "$([guid]::NewGuid().ToString())"') DO ( SET NEWGUID=%%a )

echo GUID for this node ist "%NEWGUID%"

::
:: Start loadtest script and pass id parameter 
Echo Run Loadtest Start Skript
"C:\Program Files (x86)\Geyer und Weinig\INFRA-XS\bin\wxscr32.exe"  startloadtest.gws %NEWGUID%

echo Infra-XS Agent name has been set
echo Restart node to activate
