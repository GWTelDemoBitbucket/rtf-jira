param(
     [Parameter(Mandatory=$true)][String]$astarttime,
     [Parameter(Mandatory=$true)][String]$asteptime,
     [Parameter(Mandatory=$true)][Int]$asteps,
     [Parameter(Mandatory=$true)][String]$aimagename,
     [Parameter(Mandatory=$true)][Int]$anodecount,
     [Parameter(Mandatory=$true)][String]$aenvironment="prod",
     [Parameter(Mandatory=$true)][Int]$ascripttimeout=600,
     [Parameter(Mandatory=$true)][String]$apoolname="dvagloadtest1"
     )
# Environments: "prod"    - Produktion
#                "abnahme" - Abnahme
#				"hv"      - Entwicklung
#
# Run loadtest now for 2 Steps
# runloadtest "05.02.2020 12:45" "05.02.2020 12:50" 2 "Infra-XS_Agent_21" 10 "prod"
#
# https://www.sapien.com/blog/2018/03/22/storing-powershell-variables-in-external-files/
#Skript for Loadtest
#

Function Log {
    param(
        [Parameter(Mandatory=$true)][String]$msg
    )
    $timestamp = (Get-date -f 'yyyy-MM-dd HH:mm:ss')
    Add-Content $logfile "$timestamp $msg"

	Write-Host $msg
}

Function Taskwait {
  # Show task status and wait for tasks to be finished
  $out = ( az batch job task-counts show --job-id $azcloudjobname --query "running" ) | Out-String
  Write-host "active task-count $out"	
  $finished = ($out -match "X")	

  while (!$finished) {
    Start-Sleep -s 30
    $out = ( az batch job task-counts show --job-id $azcloudjobname --query "running" ) | Out-String
    Write-Host "active task-count is $out"
    # $finished = ($out -match "0")	
	$finished = ( [convert]::ToInt32($out.trim(), 10) -eq 0 )
  }
  $out = $out.trim()
  log "finished, [$out] active Tasks"
}

Function RunStep {
    param(
        [Parameter(Mandatory=$true)][Int]$lnodecount,
        [Parameter(Mandatory=$true)][Int]$lparallel,
        [Parameter(Mandatory=$true)][Int]$llaps,
        [Parameter(Mandatory=$true)][String]$lstepname,
        [Parameter(Mandatory=$true)][String]$llogoff,
        [Parameter(Mandatory=$true)][String]$lportalsuffix
    )

	if ($lportalsuffix -eq "prod") {
	  $lportalsuffix = ""
	}
	
	TaskWait
	
    log "Start $lstepname with [$lnodecount] nodes for portal [$lportalsuffix]"
	log "Step executes [$lparallel] parallel for [$llaps] laps"
	
	# Commandline parameter have to be concatenated to be able to pass all options
	#
	$lparam_run = """$lparallel/$llaps/$i/$gscripttimeout/$gheadless"""
    $lparam_signon = """$llogoff/0""" # value has to be qouted to be accepted
    $lparam_scriptname = "dvag/jira.gws"
	
    for ($i=0;$i -lt $lnodecount;$i++) { 
       $lguid = [guid]::newGuid()   
       az batch task create `
        --task-id dvagloadtest$lstepname$i `
        --job-id $azcloudjobname `
        --command-line "cmd /c c:\runloadtest.bat $lguid $lparam_run $lstepname $lparam_signon $lparam_scriptname $lportalsuffix" 	
    #    --command-line "cmd /c c:\runloadtest.bat loadtest $parallel $loops $i Step1 -abnahme" 
    #    --command-line "cmd /c c:\runloadtest.bat node$i" 
    #    --command-line "cmd /c 'printenv | grep AZ_BATCH; sleep 90s'"     
    }

    if ($?) {
      Log "az batch task create successful [$azcloudjobname]"
    } else {
      log "az batch task create failed [$azcloudjobname]"
      return
    }

    Taskwait
    log "End $lstepname"
}

Function DeleteTasks{
    param(
        [Parameter(Mandatory=$true)][Int]$lnodecount,
        [Parameter(Mandatory=$true)][String]$lstepname
    )

	# Delete all tasks
	# 
	Log "Delete tasks for $lstepname"
	for ($i=0;$i -lt $lnodecount;$i++) { `
	   az batch task delete --yes `
		--task-id dvagloadtest$lstepname$i `
		--job-id $azcloudjobname 
	}

	if ($?) {
	  Log "az batch task delete $lstepname successful [$azcloudjobname]"
	} else {
	  log "az batch task delete $lstepname failed [$azcloudjobname]"
	  return
	}
}

Function WaitFor{
    param(
        [Parameter(Mandatory=$true)][String]$ltriggertime,
        [Parameter(Mandatory=$true)][int]$lminwaittime
    )

    if ( $ltriggertime -eq "NOW") {
      $ltriggertime = ""
    }

    #Get a DateTime object for the current time
    $lcurrentDate = get-date
	
    #Get a DateTime object for the Trigger Time
	if ($ltriggertime -ne "") { $ltriggerDate = get-date $ltriggerTime }
	  else { $ltriggerDate = get-date }

    #Subtract the Current Date from the Trigger Date to get a TimeSpan object
    #representing the amount of time you need to sleep
    $ltimeToSleep = $ltriggerDate - $lcurrentDate;

    #Convert the timespan to seconds for use as an argument in the start-sleep command
    $lsecondsToSleep = $ltimeToSleep.TotalSeconds;

    $lsecondsToSleep = [int]$lsecondsToSleep
    # echo $lsecondsToSleep

	Log "Waiting for [$ltriggertime]"
	
    #Start sleeping
    if ( [int]$lsecondsToSleep -gt 0) {
      
	  Write-Host "Waiting for [$ltriggertime]"
      Write-Host "Waiting for [$lsecondsToSleep] seconds"    
      start-sleep -seconds $lsecondsToSleep;
    } else {
      Write-Host "Waiting for [$lminwaittime] seconds"    
      start-sleep -seconds $lminwaittime
    }
}


$logfile = "./log/log-$(Get-date -f 'yyyyMMdd-HHmmss').txt"

$azcloudaccount = "gwcloud"
$azcloudresourcegroup = "dvag2019"
$azcloudpoolname = "dvagloadtest2" # TODO: configureme
$nodecount = 1 # 200
$azcloudjobname = "dvagloadtestjob"
$azimagename = ""
$gheadless = 1 # Run headleaa
$gscripttimeout = 600
#--------------------------------------
Log "---- Start ----"

Log "Params [$astarttime] Build Pool [$asteptime]  [$anodecount] nodes [$aimagename] Image [$apoolname] Poolname [$ascripttimeout] Scripttimeout"

$nodecount = $anodecount
$azimagename = $aimagename
$azcloudpoolname = $apoolname
$gscripttimeout = $ascripttimeout

WaitFor "$astarttime" 30

# once "az login"
# az login -u -p

# sign in to azure batch
Log "login to batch account [$azcloudaccount]"
az batch account login `
    --name $azcloudaccount `
    --resource-group $azcloudresourcegroup 
#    --shared-key-auth
if ($?) {
  Log "az batch login successful [$azcloudaccount,$azcloudresourcegroup]"
} else {
  log "az batch login failed [$azcloudaccount,$azcloudresourcegroup]"
  return
}

# Json Method
# Edit poolconfiguration.json
(Get-Content poolconfiguration.json) | `
Foreach-Object {$_ -replace "POOLNAME","$azcloudpoolname" } | `
Foreach-Object {$_ -replace "NODECOUNT","$nodecount" } | `
Foreach-Object {$_ -replace "IMAGENAME","$azimagename" } | `
 Out-File _poolconfiguration.json
# Create a pool of nodes	
# target node count 400
Log "create az batch pool [$azcloudpoolname] with Json"
Log "nodes are created with image [$azimagename]"
az batch pool create `
    --json-file _poolconfiguration.json
#
#	
if ($?) {
  Log "az batch pool create successful [$azcloudpoolname,$nodecount]"
} else {
  log "az batch pool create failed [$azcloudpoolname,$nodecount]"
  return
}

# Show pool status and wait for pool to be steady
$out = (az batch pool show --pool-id $azcloudpoolname `
    --query "allocationState" ) | Out-String

log "pool status $azcloudpoolname is $out"	

$poolisready = ($out -match "steady")	
while (!$poolisready) {
  Start-Sleep -s 10
  $out = (az batch pool show --pool-id $azcloudpoolname `
      --query "allocationState" ) | Out-String
   Write-Host "pool is $out"
   $poolisready = ($out -match "steady")	
}

log "pool $azcloudpoolname is $out"	
log "The pool nodes are starting"

# Wait for all nodes to be available in status idle
# http://jmespath.org/tutorial.html
# the expression assumes one pool
$out = (az batch pool node-counts list --query "[0].dedicated.idle" ) | Out-String
$out = $out.trim()
log "pool $azcloudpoolname has [$out] available nodes"

$poolisready = ($out -match "$nodecount")
while (!$poolisready) {
  Start-Sleep -s 15
  $out = (az batch pool node-counts list --query "[0].dedicated.idle" ) | Out-String
  Write-Host "Available node count is $out"
   $poolisready = ($out -match "$nodecount")	
}

log "pool $azcloudpoolname is available with [$nodecount] nodes"

# Guard time (multiply by nodecount)
# Start-Sleep -s 5*$nodecount

# Create Job
az batch job create `
    --id $azcloudjobname `
    --pool-id $azcloudpoolname	
if ($?) {
  Log "az batch job create successful [$azcloudjobname]"
} else {
  log "az batch job create failed [$azcloudjobname]"
  return
}

# Loadtest
#  Nodecount Parallel Laps Stepname Portalsuffix
#  DONE: Add Scheduling
#  DONE: make step names sortable : step01....
WaitFor "$asteptime" 30

#  Nodecount Parallel Laps Stepname Portalsuffix

if ($asteps -gt 0) { RunStep 5 1 2 "Step01" 0 "$aenvironment" }
if ($asteps -gt 1) {  RunStep 10 1 2 "Step02" 0 "$aenvironment" }
if ($asteps -gt 2) {  RunStep 20 2 2 "Step03" 0 "$aenvironment" }
if ($asteps -gt 3) {  RunStep 30 2 2 "Step04" 0 "$aenvironment" }
if ($asteps -gt 4) {  RunStep 40 2 2 "Step05" 0 "$aenvironment" }
if ($asteps -gt 5) {  RunStep 50 2 2 "Step06" 0 "$aenvironment" }
if ($asteps -gt 6) {  RunStep 60 2 2 "Step07" 0 "$aenvironment" }
if ($asteps -gt 7) {  RunStep 70 2 2 "Step08" 0 "$aenvironment" }
if ($asteps -gt 8) {  RunStep 80 2 2 "Step09" 0 "$aenvironment" }
if ($asteps -gt 9) {  RunStep 90 2 2 "Step10" 0 "$aenvironment" }
if ($asteps -gt 10) {  RunStep 100 2 2 "Step11" 0 "$aenvironment" }
if ($asteps -gt 11) {  RunStep 120 2 10 "Step12" 0 "$aenvironment" }
if ($asteps -gt 12) {  RunStep 140 2 2 "Step13" 0 "$aenvironment" }
if ($asteps -gt 13) {  RunStep 160 2 2 "Step14" 0 "$aenvironment" }
if ($asteps -gt 14) {  RunStep 180 2 2 "Step15" 0 "$aenvironment" }
if ($asteps -gt 15) {  RunStep 200 2 2 "Step16" 0 "$aenvironment" }

# 
 WaitFor "NOW" 300
 RunStep $nodecount 1 1 "Stepclose" 1 "$aenvironment"
#
WaitFor "NOW" 300

# Delete all tasks
# 
if ($asteps -gt 0) { DeleteTasks 5 "Step01"  }
if ($asteps -gt 1) {  DeleteTasks 10 "Step02" }
if ($asteps -gt 2) {  DeleteTasks 20 "Step03" }
if ($asteps -gt 3) {  DeleteTasks 30 "Step04" }
if ($asteps -gt 4) {  DeleteTasks 40 "Step05" }
if ($asteps -gt 5) {  DeleteTasks 50 "Step06" }
if ($asteps -gt 6) {  DeleteTasks 60 "Step07" }
if ($asteps -gt 7) {  DeleteTasks 70 "Step08" }
if ($asteps -gt 8) {  DeleteTasks 80 "Step09" }
if ($asteps -gt 9) {  DeleteTasks 90 "Step10" }
if ($asteps -gt 10) {  DeleteTasks 100 "Step11" }
if ($asteps -gt 11) {  DeleteTasks 120 "Step12" }
if ($asteps -gt 12) {  DeleteTasks 140 "Step13" }
if ($asteps -gt 13) {  DeleteTasks 160 "Step14" }
if ($asteps -gt 14) {  DeleteTasks 180 "Step15" }
if ($asteps -gt 15) {  DeleteTasks 200 "Step16" }
                       DeleteTasks $nodecount "Stepclose"
#
#
#

# Delete a job	
az batch job delete --yes `
    --job-id $azcloudjobname	
if ($?) {
  Log "az batch job delete successful [$azcloudjobname]"
} else {
  log "az batch job delte failed [$azcloudjobname]"
  return
}

# Delete the pool
az batch pool delete --yes `
    --pool-id $azcloudpoolname 
#	
if ($?) {
  Log "az batch pool delete successful [$azcloudpoolname,$nodecount]"
} else {
  log "az batch pool delete failed [$azcloudpoolname,$nodecount]"
  return
}

	
<#
Loadtest
add tasks for each Step
Step 1..x
  Params
  Starttime, Nodecount, Parallel, Laps, Stepname, Portalsuffix
  
  if (time - Starttime > 0) 
    Start-Sleep -seconds (time - Starttime)

  for 1 to nodecount do
    $guid = [guid]::newGuid()
    create task$Stepname$count
	--command-line c:\runloadtest.bat $guid $parallel $laps $count $Stepname $Portalsuffix
	
  Wait for all Tasks to be finished
  # Wait 120s for transmit of data

Guard time (multiply by nodecount)

Remove all tasks
Remove the pool
#>
