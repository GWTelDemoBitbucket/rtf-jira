# https://github.com/ekapope/Combine-CSV-files-in-the-folder/blob/master/Combine_CSVs.py
# C:/Users/admin/Documents/GW/Azure/Loadtest/lasttest_DVAG_abnahme_olfmess.20200205/CSV/lte885c6fc-d199-4c47-b9cf-d1105931fcb4_616c56ce-1dc7-49fe-bb8c-e0bd1a46950c.csv

import os
import glob
import pandas as pd

os.chdir("C:/Transfer/work/")
extension = 'csv'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
#combine all files in the list
combined_csv = pd.concat([pd.read_csv(f) for f in all_filenames ])
#export to csv
combined_csv.to_csv( "combined_csv.csv", index=False, encoding='utf-8-sig')