/*
 � Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
   All rights reserved -- Alle Rechte vorbehalten
   GW-TEL� and INFRA-XS� are registered trademarks of Geyer & Weinig GmbH
   GW-TEL� und INFRA-XS� sind eingetragene Warenzeichen der Firma Geyer & Weinig GmbH

 Starting the loadtest

 Parameter:

 We only have 6 args in gws script so the last one contains all the rest
 
 arg1 : SER_ID
 arg2 : <Parallel runs>/<Number of runs in one thread>/<Node counter>/<Timeout in ms>/<Headless>/<Project>
 arg3 : Step name
 arg4 : <Logoff>/<Login>
 arg5 : <target script>
 arg6 : <Portal suffix>

 <Headless> : 0 or 1, default is 1
 <Project>  : For searching in the ini file (see ini file adz.ini)
              "empty" : Normal system
              "test"  : Testsystem
 <Logoff>   : 0 (no logoff) or 1 (logoff at the end of the queue)
 <Login>    : 1 (always login, so we make always a logoff too), 0 (use session from last run)

*/

string g_username = "";
string g_password = "";

//
string g_nodeid = "s12agent2";
string g_wsname = "";

// Loadtest - Loadtest.config
string g_scriptName = "runloadtest.gws";
int g_Offset = 30000;
int g_Parallel = 1;
string g_stepname;
int g_agentindex = 1;
string g_portalsuffix = "";
string g_targetscript = "";
int    g_timeout = 0;
string g_loadtest_processname = "wxjira";
int    g_laps;
int    g_headless = 1;
int    g_logoff = 1;
int    g_login = 1;
string g_ser_id = "";
string g_project = "";

//
int main() {
  int result = 0;
  int l_index = 0;
  int l_procid;
  string l_sres;
  int l_res;
  
 if (fileloadscript("AppLib1.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLib1.gws> missing");
    return 1;
  }
  l_sres = LibInit("x!x!x");
  if ( l_sres != "" ) {
    ReportError(l_sres);
    return 1;
  }

  if (fileloadscript("AppLibUpdate.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLibUpdate.gws> missing");
    return 1;
  }
  if (!UpdateInit()) {
    ReportError( "Fehler bei der Initialisierung der Aktualisierungsbibliothek <AppLibUpdate.gws>" );
    return 5;
  }

  // Report info about current user and display size
  ReportSystemInfo(0);

  if (fileloadscript("AppLibWebdriver.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLibWebdriver.gws> missing");
    return 1;
  }

  // Check if there is a repository error then try to retrieve repository
  if ( fileexist( "REPOSITORY")) {
    fileremove("REPOSITORY");
    l_res = Get_Repository(0);
    if (l_res < 0) {
      // Fehler beim Abruf
      ReportError( "Error Repository retrieval");
    } else {
      ReportInfo( "Repository retrieval successful");
    }
  }

  g_ser_id = arg1;

  // each node gets a guid on first call
  filegetprfstr("services", "defnodeid", &g_nodeid);

  g_Parallel   = getFromList(arg2, "/", 1, 1);
  g_laps       = getFromList(arg2, "/", 2, 1);
  g_agentindex = getFromList(arg2, "/", 3, 1);
  g_timeout    = getFromList(arg2, "/", 4, 1);
  g_headless   = getFromList(arg2, "/", 5, 1);
  g_project    = strGetFromList(arg2, "/", 6, "");

  g_stepname = arg3; // Step name

  g_logoff = getFromList(arg4, "/", 1, 1);
  g_login  = getFromList(arg4, "/", 2, 1);

  g_targetscript = arg5;
  g_portalsuffix = arg6;

  if (g_targetscript == "") {
    ReportError("Cannot run without a tagret script!");
    result = 1;
  } else {
    // remove processes from previous run if any
    clean_processes(0);

    if ( g_Parallel > 0 ) {
      for ( l_index = 0; l_index < g_Parallel; l_index++) {
        l_procid = RunScript ( "", l_index);
      }
    } else {
      l_procid = RunScript ( "", -1) ;
    }
    delay(10000);

    // The started scripts should finish within g_timeout + guard.
    waitForEndOfProcesses(g_loadtest_processname, g_timeout, g_laps);
    clean_processes(g_logoff);
  }

  return result;
}

//
// Wait for the end of the running processes
//
void waitForEndOfProcesses(string process, int timeout, int laps) {
   int l_waittime = 1000 * timeout * laps;
   int l_ende = waitfortask( process, 0, l_waittime);
   if (l_ende) {
     ReportInfo(strform("Alle Messungen wurden in der erwarteten Zeit von [%d s] abgeschlossen.", l_waittime/1000));
   } else {
     ReportInfo(strform("Nicht alle Messungen wurden in der erwarteten Zeit von [%d s] abgeschlossen.", l_waittime/1000));
   }
}

//
// Kill processes
//
// If killwebdriver is 1 we kill the webdriver an chrome instances too
//
void clean_processes(int killwebdriver) {

  ReportInfo("Clean processes ");
  // remove processes from previous run
  _kill( g_loadtest_processname);
  if (killwebdriver == 1) {
    // Set browser and version so the webdriver library can calculate the webdriver name
    g_current_browser = "chrome";
    g_browser_version = get_browser_version();
    _kill(webdriver_name(0));
    _kill("chrome");
  }
}

//
// Kill a task
//
void _kill(string atask) {
  int l_count = 10; // Maximum 10 tries
  while (!waitfortask(atask, 0, 1, -1) && (l_count > 0)) {
    execstop(atask, -1);
    l_count--;
  }
  ReportInfo(strform("Task [%s,%d] killed.",atask, ));
}

//
// Get a string value from a / separated list
// <value1>/<value2>/ .. /<valueN>
//
// No index check!
//
// list      : List wirh values
// separator : Separator between elements
// index     : index of the wanted element
// def       : default value
//
string strGetFromList(string list, string separator, int index, string def) {
  string result = def;
  int array = ArrFromString(list, separator);
  string element = "";
  arrfind(array, asstring(index), &element);
  if (element != "") result = element;
  arrdestroy(array);
  return result;
}

//
// Get an integer value from a / separated list
// <value1>/<value2>/ .. /<valueN>
//
// No index check!
//
// list      : List wirh values
// separator : Separator between elements
// index     : index of the wanted element
// def       : default value
//
int getFromList(string list, string separator, int index, int def) {
  int result = def;
  string element = strGetFromList(list, separator, index, "");
  if (element != "") result = strval(element);
  return result;
}

//
// Get the application for the runs
//
string getProgAlias() {
  string wxscr32 = argv(0);
  string result = strform("%s\\%s.exe", filefullpath(strappslash(filegetdir(1))), g_loadtest_processname);
  // Check if the files are ok, if we have a new wxscr32 or no target process
  if (!filecomp(wxscr32, result)) {
    // Dateien unterscheiden sich, also Orginal in Alias kopieren
    if (!filecopy(wxscr32, result)) {
      ReportWarning(strform("Cannot create program alias %s", result));
      // We take the wxscr32.exe
      result = wxscr32;
      // Change process name to wait for
      g_loadtest_processname = FileGetPart(result, 1);
    }
  }
  return result;
}

int RunScript ( string aParam, int aParallel) {

  string l_workdir = ExpandDataPath("scripts");
  string l_wxscr32 = getProgAlias();
  string l_script = "scriptrunner.gws";
  string l_params;
  string l_agent;
  int    l_procid;

  string l_commandline = strform("%s %s", l_wxscr32, l_script);

  // timeout on run : look to applc to make processes differentiable -- multisession ID, process ID
  // add timeout to process call
  delay( 250); // spread start of script to avoid "unfinished counters"

  if ( aParallel > -1) {
    g_wsname = strform("%s", g_nodeid);
    l_agent = strform("%s.%d", g_nodeid, aParallel);
  } else {
    g_wsname = g_nodeid;
    l_agent = strform("%d", g_agentindex);
  }
  // The XXXXX will be replaced with the valid cronstring by the scriptrunner.gws
  l_params  = strform("%s!x!x!InvocationInfo=InvocationInfo,ser_id=%s,applcmeasurement=XXXXX", g_nodeid, g_ser_id);

  // Parameter to scriptrunner.gws are:
  // arg1 : Script to call (dvag/jira.gws)
  // arg2 : Number of runs
  // arg3 : Normal arg 1 parameter (<WSName>!x!x)
  // arg4 : project, empty string or abnahme, entwicklung (without -)
  // arg5 : headless, 0 or 1
  // arg6 : Rest of parameter with | as separator, we only have 6 args
  //        <step name>|<agent>|<timeout>|<logoff>|<login>|<ser_id>|<agent index>|<killlist>

  l_params = strform("\"%s\" %d %s \"%s\" %d \"%s|%s|%u|%d|%d|%s|%d|%s\"",
           g_targetscript, 
					 g_laps, 
					 l_params, 
					 g_portalsuffix, 
					 g_headless, 
					 g_stepname, l_agent, g_timeout, g_logoff, g_login, g_ser_id, g_agentindex, g_project);
  l_commandline = l_commandline + " " + l_params;

  // put to pre process
  // copy account from default entry
  string l_portalsuffix = g_portalsuffix;
  if (!(l_portalsuffix == "")) {
    l_portalsuffix = "-" + l_portalsuffix;
  }
  copy_account(strform("jira%s", l_portalsuffix), "s12agent5");

  // Call script runner
  l_procid = execcmd(l_commandline, 0, l_workdir);

  ReportInfo(strform("Script [scriptrunner] executed. Params [%s]", l_params));

  return l_procid;
}


string copy_account(string application, string awsname) {
  string appl = application;

  // Benutzerkennung aus Zugangsverwaltung einlesen
  if (filegetacct(strform("%s@%s", appl, awsname), &g_username, &g_password)) {
    fileputacct( strform("%s@%s", appl, g_wsname), g_username, g_password);
  }
  return "";
}


Get_Repository(int aFilesOnly)
{
  string l_adminName    = "";
  string l_adminPort    = "";
  string l_adminSSL     = "";
  string l_adminPortDef = "80";
  string l_repository   = "";
  int l_res = 0;

  // Repository update in einer MultiSession Umgebung nur von der Konsole

    // Informationen für HTTP(s)-Verbindung ermitteln
    filegetprfstr( "CustomScripts","adminserver",&l_adminName );
    filegetprfstr( "CustomScripts","adminserverport",&l_adminPort );
    if (l_adminPort == "") l_adminPort = "80";
    filegetprfstr( "CustomScripts","adminserverssl",&l_adminSSL );
    if (l_adminSSL == "") l_adminSSL = "0";
    if ( strval( l_adminSSL ) ) {
      l_adminPortDef = "443";
    }
    l_repository = strform( "%s://%s%s/repository",
                                      strval( l_adminSSL ) ? "https" :"http",
                                      l_adminName,
                                      l_adminPort != l_adminPortDef ? strform( ":%s",l_adminPort ) : "" );

    // get normal repository
    l_res = _PerformUpdateRepository( "default","c:\\programdata\\Geyer und Weinig\\Infra-XS\\default\\repository",l_repository+"/default","localhost",0 );

  return l_res;
}


int _PerformUpdateRepository( string aTenant,string aBasePath,string aRepository,string a_SubRepository,int aQuiet )
{
  int    l_res;
  string l_oldMD5;
  string l_newMD5;
  string l_command;
  string l_proxypasswd;
  string l_options;
  string l_adminserver;
  string l_fingerprint    = "";
  string l_fingerprintdef = "";
  string l_proxytype      = "";
  string l_proxyserver    = "";
  string l_proxyuser      = "";
  string l_proxydef       = "";
  int    l_result         = 0;
  string l_iniFile        = strappslash( aBasePath ) + "laststate.ini";
  string l_target         = strappslash( aBasePath ) + a_SubRepository;
  string l_checkFile      = strappslash( l_target ) + ".hg\\dirstate";
  string l_repository     = aRepository + "/" + a_SubRepository;
  string l_logFile        = strappslash( ExpandDataPath( "logs" ) ) + strform( "repository.%s.log",strlower( g_wsname ) );
  string l_hgexe          = gethgexe();

  // Proxyinformationen ermitteln
  if ( !filegetprfstr( "Settings","proxytype",&l_proxytype ) || (l_proxytype == "") )
    l_proxytype = "0";
  // Mercurial unterst�tzt nur HTTP
  if ( strval( l_proxytype ) == 3 ) {
    if ( filegetprfstr( "Settings","proxyserver",&l_proxyserver ) && (l_proxyserver != "") ) {
      l_proxydef = strform( "--config http_proxy.host=%s ",l_proxyserver );
      filegetprfstr( "Settings","proxyuser",&l_proxyuser );
      if ( filegetacct( StrBefore( l_proxyserver,":" ),&l_proxyuser,&l_proxypasswd,&l_options ) ) {
        l_proxydef = l_proxydef + strform( "--config http_proxy.user=%s --config http_proxy.passwd=XxXxX ",l_proxyuser );
      }
    }
  }

  // Fingerabdruck f�r SSL-Verbindungen
  if ( filegetprfstr( "Settings","fingerprint",&l_fingerprint ) && (l_fingerprint != "") ) {
    filegetprfstr( "CustomScripts","adminserver",&l_adminserver );
    l_fingerprintdef = strform( "--config hostfingerprints.%s=YyYyY ",l_adminserver );
  }

  // Wenn kein Fingerabdruck vorhanden ist aber SSL verwendet werden soll auf
  // die Zertifikatspr�fung verzichten
  if ( (strpos( l_repository,"https:" ) > 0) && (l_fingerprintdef == "") ) {
    l_fingerprintdef = "--insecure ";
  }

  l_fingerprintdef = "--insecure ";
    // Auszuf�hrenden Befehl berechnen
  if ( fileexist( l_target,1 ) ) {
    l_command = l_hgexe + " pull " + l_fingerprintdef + l_proxydef + "-u " + l_repository + " -R \"" + l_target + "\"";
  } else {
    l_command = l_hgexe + " clone " + l_fingerprintdef + l_proxydef + l_repository + " \"" + l_target + "\"";
  }

  ReportInfo( strform("Command [%s]", l_command));

  // Meldung in Protokolldatei ausgeben und Aktualisierung des Versionskontrollsystems durchf�hren
  execcmd( "\"" + sysgetenv( "ComSpec" ) + "\" /c \"echo " + strdtime( "%Y-%m-%d-T%H:%M:%S",systime() ) + " " + l_command + " >> \"" + l_logFile + "\"\"",10 + 256 + 1024 );
  l_res = execcmd( "\"" + sysgetenv( "ComSpec" ) + "\" /c \"" + strsetstr( strsetstr( l_command,"XxXxX",l_proxypasswd ),"YyYyY",strunscramble( l_fingerprint ) ) + " >> \"" + l_logFile + "\" 2>&1\"",10 + 256 + 1024, "", 60000 );
  ReportInfo( strform("Repository retrieval Result [%d]", l_res));
  if (( l_res == 255 ) || (l_res <0)) {
    filelog( 0, "Fehler");
    l_res = -1;
    _CreateControlFile("REPOSITORY");
  }
  if ( l_res >= 0 ) {
    // Eine ge�nderte Pr�fsumme des Versionskontrollsystems zeigt an, dass sich etwas ge�ndert hat
    filegetprivstr( l_iniFile,a_SubRepository,"checksum",&l_oldMD5 );
    if (!fileinfo( l_checkFile,"md5sum",&l_newMD5 ) || (l_oldMD5 != l_newMD5)) {
      ReportInfo( strform( "�en:Updating data path from repository�de:Aktualisiere Datenpfad aus Versionskontrollsysteme�� <%s>",
                           l_target ) );
      FileCopyFiltered( l_target,strsetstr( aBasePath,"\\repository","" ),"^\\.hg",1,1 );
      fileputprivstr( l_iniFile,a_SubRepository,"checksum",l_newMD5 );
    }
  } else {
    l_result = -1;
    if (!aQuiet) {
      ReportError( strform( "�en:Updating of repository�de:Aktualisierung von Versionskontrollsystem�� <%s> �en:failed (Error�de:fehlgeschlagen (Fehler�� <%d>)",
                            l_repository,l_res ) );
    }
  }

  return l_result;
}


_CreateControlFile( string a_file ) {
  int    l_file;

  if (!fileexist( a_file )) {

    l_file = filecreat( a_file );
    if (l_file) {
      filewrite( l_file, "\n" );
      fileclose( l_file );
    }
  }
}

