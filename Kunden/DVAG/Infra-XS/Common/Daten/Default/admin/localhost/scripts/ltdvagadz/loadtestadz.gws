/*
 � Copyright 1998-2020 Geyer & Weinig EDV-Unternehmensberatung GmbH
   All rights reserved -- Alle Rechte vorbehalten
   GW-TEL� and INFRA-XS� are registered trademarks of Geyer & Weinig GmbH
   GW-TEL� und INFRA-XS� sind eingetragene Warenzeichen der Firma Geyer & Weinig GmbH

 Load test script

 Required libraries:
   - AppLib1.gws
   - AppLibAzure.gws

*/

string g_scriptName = "loadtestadz.gws";
string g_service    = "ltadz.";

string g_azcloudaccountname;
string g_azresourcegroup;
string g_azsubscription;
string g_azresourcenetwork;
string g_galleryname;
string g_imageversion;
string g_poolname;
int    g_nodecount;
string g_imagename;
string g_azjobname;
string g_testscript;
int    g_timebetweensteps;
string g_stepnamedescriptor;
int    g_stepnumber;
int    g_useexisting;
string g_project;         // Project for reading data from ini file (for running script)

//
// Main
//
int main() {
  int result = 0;
  int l_count = 0;

  string l_sres;

  if (fileloadscript("AppLib1.gws") == 0) {
     filelog(3, g_scriptName+": script library <AppLib1.gws> missing");
     return 1;
  }
  l_sres = LibInit(arg1);
  if ( l_sres != "" ) {
    ReportError(l_sres);
    return 1;
  }

  // Report info about current user and display size
  ReportSystemInfo(0);

  // Scriptlibrary for some special functions
  if ( fileloadscript( "AppLib2.gws" ) == 0 ) {
    filelog( 3,g_scriptName + ": INFRA-XS Script-Bibliothek <AppLib2.gws> nicht vorhanden - Script wird beendet" );
    return 1;
  }
  if (fileloadscript("AppLibFunctions.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLibFunctions.gws> missing");
    return 1;
  }

  if (fileloadscript("AppLibAzure.gws") == 0) {
    ReportError(g_scriptName+": script library <AppLibAzure.gws> missing");
    return 1;
  }

  g_az_debug = 1; // Debugging in library

  AppStart();
  az_start();

  if (get_loadtestparams() == 0) {
    ReportError(g_scriptName+": the loadtest parameters are not complete");
    return 1;
  }

  _CounterStart( "Run" );
  // Login to Azure
  if (LoginToAzure()) {
    // Build the new pool
    if (BuildNewPool()) {
      // Build the job
      if (BuildJob()) {
        if (g_stepnumber > 0) {
          result = runsteps();
        }
        DeleteJobs();
      }
      DeletePool(); // Delete pool if we are not in test mode
    } else {
    // Remove Pool checking if there is a pool
    DeletePool();
  }
  }
  if (result == 1) {
    _CounterStop("Run");
  } else {
    _CounterFail("Run");
  }

  return AppStop(1-result);
}

//
// Login to Azure
//
int LoginToAzure() {
  int result = 1;
  _CounterStart( "Batchlogin");
  ReportInfo(strform("Login to batch account [%s]", g_azcloudaccountname)); 
  if (az_batchlogin( g_azcloudaccountname, g_azresourcegroup, g_azsubscription)) {
    _CounterStop( "Batchlogin");
  } else {
    _CounterFail( "Batchlogin");
    result = 0;
  }
  return result;
}

//
// Build a new pool or reuse an existing one
//
int BuildNewPool() {
  int result = 1;
  _CounterStart("BuildPool");
  setjsontemplate(filefullpath(strappslash(filegetdir(0)) + "ltdvagadz/poolconfiguration.json"));

  // useexisting: check if the pool is available
  int l_poolindex = az_checkpoolname(g_poolname, 0);  // Don't show error if pool doesn't exist
  if ((g_useexisting > 0) && (l_poolindex > -1)) {
    // use available pool
    ReportInfo(strform("Existing pool [%s] will be used", g_poolname));
    _CounterStop( "BuildPool");
  } else {
    // Build a new pool
    // Building a pool cannot take forever
    int l_timeout = calculatetimeout(g_nodecount, az_gettotaltime());
    setazuretimeout(l_timeout);
    // TODO: check for existing pool and delete
    int l_result = az_poolcreate(g_poolname, g_nodecount, g_imagename, g_azresourcegroup, g_azsubscription, g_azresourcenetwork,
                                 g_galleryname, g_imageversion);
    if (l_result == 1) l_result = az_waitpool( g_poolname, g_nodecount);
    ReportInfo(strform("The pool [%s] has [%s] available nodes", g_poolname, azurepoolnodecount()));
    ReportInfo(strform("Pool build time [%d] remaining timeout [%d]", get_poolpreparationtime(), SysTimeoutGet()));
    if (l_result == 1) {
      _CounterStop( "BuildPool");
      // Decide to abort or salvage the rest of the available time
      if ((SysTimeoutGet() == 0) && (g_nodecount == azurepoolnodecount()) && (g_useexisting == 0) ) {
        // salvage the run but shorten the steps
        g_stepnumber = g_stepnumber - 2; // add calculation of steptime
        ReportInfo(strform("The run has to be shortended. [%d] steps", g_stepnumber));
      }
    } else {
      _CounterFail( "BuildPool");
      result = 0;
    }
  }

  return result;
}

//
// Delete the pool if we are not in test mode
//
int DeletePool() {
  int result = 1;
  int l_result = 0;
  if ((g_useexisting == 0) || (g_useexisting == 2)) {
    _CounterStart( "DeletePool");
  // check if Pool exists
  l_result = az_checkpoolname(g_poolname, 0);
    if ( (l_result > -1) && (az_deletepool(g_poolname))) {
      _CounterStop( "DeletePool");
    } else {
      _CounterFail( "DeletePool");
      result = 0;
    }
  }
  return result;
}

//
// Build the job
//
int BuildJob() {
  int result = 1;
  _CounterStart("BuildJob");
  if (az_createjob(g_poolname, g_azjobname)) {
    _CounterStop("BuildJob");
  } else {
   _CounterFail("BuildJob");
   result = 0;
  }
  ReportInfo(strform( "[%d] seconds to complete run", az_timeremaining() ));
  return result;
}

//
// Delete the jobs
//
int DeleteJobs() {
  int result = 1;
  _CounterStart( "DeleteJob");
  if (az_deletejob(g_azjobname)) {
   _CounterStop( "DeleteJob");
  } else {
    _CounterFail( "DeleteJob");
    result = 0;
  }
  return result;
}


int calculatetimeout( int anodecount, int aruntimetotal) {
  int l_result = 0;
  int l_value = anodecount * 3; // Offset per node

  if (( anodecount > 0)   && ( anodecount < 21 ))  l_value = l_value + 600;
  if (( anodecount > 20)  && ( anodecount < 51 ))  l_value = l_value + 800;
  if (( anodecount > 50)  && ( anodecount < 101 )) l_value = l_value + 1200;
  if (( anodecount > 100) && ( anodecount < 201 )) l_value = l_value + 1800;

  if (l_value > aruntimetotal) l_result = l_value;
    else l_result = aruntimetotal;

  return l_result;
}

void _CounterStart(string aCounterName) {
  ReportInfo(strform("CounterStart %s", aCounterName));
  CounterStart(aCounterName);
  CounterParams(g_stepnamedescriptor, "");
}

void _CounterStop(string aCounterName){
  ReportInfo(strform("CounterStop %s", aCounterName));
  CounterStop(aCounterName);
}

void _CounterFail(string aCounterName){
  ReportInfo(strform("CounterFail %s", aCounterName));
  CounterFail(aCounterName);
}



int runsteps(){
  int l_steparray = arrcreate();
  int l_result;
  int l_count;
  int l_finished;
  string l_scriptname;
  string l_params;
  string l_stepname;
  string l_param_stepname;
  int l_starttime;
  int l_stepparams;
  int l_nodecount;
  int l_parallel;
  int l_laps;
  int l_steps;
  int l_steptime;
  string l_serid;
  string l_killlist;
  string l_project;

  l_count = 1;
  l_finished = (l_count > g_stepnumber);
  while (!l_finished) {
    l_scriptname = g_testscript; // get parameter for step from config
    l_project    = g_project;
  
    // get params for step from config file
    l_stepname  = strform("%s%02d", g_stepnamedescriptor, l_count); 
      // extract key value pairs for current step from applc.config as a string
    l_stepparams = get_stepparams(l_stepname);
    
    if (l_stepparams > 0) {
      l_param_stepname = fl_param_get(l_stepparams, "stepname", l_stepname);
      l_nodecount   = strval(fl_param_get(l_stepparams, "nodecount", "1"));
      l_parallel    = strval(fl_param_get(l_stepparams, "parallel", "1"));
      l_laps        = strval(fl_param_get(l_stepparams, "laps", "1"));
      l_scriptname  = fl_param_get(l_stepparams, "scriptname", l_scriptname);
      l_params      = fl_param_get(l_stepparams, "params", "");
      l_steptime    = strval(fl_param_get(l_stepparams, "steptime", "300"));
      l_serid       = fl_param_get(l_stepparams, "serid", "");
      l_killlist    = fl_param_get(l_stepparams, "killlist", "");
      l_project     = fl_param_get(l_stepparams, "project", l_project);
      arrdestroy(l_stepparams);

      ReportInfo(strform("---- Step [%s] Parameters ---- [%s]", l_stepname, l_stepparams));
      ReportInfo(strform(" stepname    [%s]", l_param_stepname));
      ReportInfo(strform(" nodecount   [%d]", l_nodecount));
      ReportInfo(strform(" parallel    [%d]", l_parallel));
      ReportInfo(strform(" laps        [%d]", l_laps));
      ReportInfo(strform(" scriptname  [%s]", l_scriptname));
      ReportInfo(strform(" params      [%s]", l_params));
      ReportInfo(strform(" steptime    [%d]", l_steptime));
      ReportInfo(strform(" serid       [%s]", l_serid));
      ReportInfo(strform(" killlist    [%s]", l_killlist));
      ReportInfo(strform(" project     [%s]", l_project));

      ReportInfo(strform( "[%d] seconds to complete ", az_timeremaining()));
      // check available time before executing step
      if ( l_steptime < az_timeremaining() ) {
        _CounterStart( l_stepname);
        l_starttime = systime();
        l_result = az_runstep(g_azjobname, l_nodecount, l_parallel, l_laps, l_stepname, l_param_stepname,
                              l_params, l_scriptname, l_serid, l_steptime, l_killlist, l_project);
        ReportInfo(strform("Step [%s] duration [%d] steptime [%d]", l_stepname, systime()-l_starttime, l_steptime));
         if (l_result == 1) _CounterStop(l_stepname);
           else _CounterFail(l_stepname);
                
         add_steparray(l_steparray, l_count, l_stepname, l_nodecount);
         // Delay only if this is not the last step
         if (l_count < g_stepnumber) {
           ReportInfo(strform("Delay between Steps [%s]", g_timebetweensteps));
           delay( g_timebetweensteps);
         }
      } else {
        ReportInfo(strform(" Step [%s] not executed time to short. [%d]s remaining.",
                   l_stepname, az_timeremaining()));
      }
    }
  
    // abort, if a step has failed
    if (l_result == 1) l_count++;
      else l_count = g_stepnumber + 1;
    l_finished = (l_count > g_stepnumber);  
  }

  l_steps = ArrCount(l_steparray);
  l_count = 1;
  l_finished = (l_count > l_steps);
  while(!l_finished) {
    int l_array = ArrFromString(ArrAtString(l_steparray, StrFromInt(l_count)), ",");
    l_stepname  = fl_param_get(l_array, "stepname", "Step01");
    l_nodecount = strval(fl_param_get(l_array, "nodecount", "1"));
    arrdestroy(l_array);
    
    l_result = az_deletetasks(l_nodecount, l_stepname, g_azjobname);
    
    l_count++;
    l_finished = (l_count > l_steps); 
  }
  arrdestroy( l_steparray);

  return l_result;
}

int add_steparray( int aarray, int acount, string astepname, int anodecount){
  int l_result;
  string l_string = strform("stepname=%s,nodecount=%d", astepname, anodecount);

  l_result = arrsetobj( aarray, StrFromInt( acount), l_string);

  return l_result;
}



//  stepname=Step,nodecount=1,parallel=1,laps=2,scriptname=,params=0/1/prod

//
// Create an array with the parameters for step <a_stepname>
//
// Entries in this array are <name>=<value>
//
int get_stepparams(string a_stepname) {
  string l_fname = StrReplAll( g_configFName, "/", "\\");
         l_fname = strform("%s/%s.dat", StrBefore(l_fname, "\\"), a_stepname);
  int    result = fl_param_array(l_fname);
  ReportInfo(strform(" Config file         [%s]", l_fname));
  ReportInfo(strform("---- Parameters ---- [%s]", fl_array_to_string(result)));
  return result;
}


//
// Load the parameters for the load test from the .dat/.params files
//
int get_loadtestparams() {
  int l_result = 0;
  int array = fl_param_array(g_configFName);
  if (array > 0) {
    g_azcloudaccountname = fl_param_get(array, "azcloudaccountname", "gwcloud");
    g_azresourcegroup = fl_param_get(array, "azresourcegroup","dvag2019");
    g_azsubscription =  fl_param_get(array, "azsubscription","939fd29b-c763-4e2d-b780-a7f65eede4d2");
    g_azresourcenetwork = fl_param_get(array, "azresourcenetwork","vnet-westeurope-azurebatch");
    g_poolname = fl_param_get(array, "poolname","testpool08");
  	g_galleryname  = fl_param_get(array, "galleryname","ImageGallery_GermanyWestCentral");
  	g_imageversion = fl_param_get(array, "imageversion","53.0.0");
    g_nodecount = strval(fl_param_get(array, "nodecount", "2"));
    g_imagename = fl_param_get(array, "imagename","Infra-XS_Agent_30");
    g_azjobname = fl_param_get(array, "azjobname","jobloadtest");
    // will be overridden if there is a specific scriptname for a step
    g_testscript = fl_param_get(array, "scriptname","dvag/lt-jira.gws");
    g_timebetweensteps = strval(fl_param_get(array, "timebetweensteps", "120")) * 1000;
    // which steps to select from config by name
    g_stepnamedescriptor = fl_param_get(array, "stepnamedescriptor", "LoadtestStep");
    // how many steps to execute
    g_stepnumber = strval( fl_param_get(array, "stepnumber", "1"));
    // how much time for execution
    az_settotaltime(strval( fl_param_get(array, "totalruntime", "900")));
    // is this a useexisting
    g_useexisting = strval( fl_param_get(array, "useexisting", "0"));
    // Project for access to the ini file for the script
    g_project = fl_param_get(array, "project","empty");

    ReportInfo(strform(" Config file         [%s]", g_configFName));
    ReportInfo(strform("---- Parameters ---- [%s]", fl_array_to_string(array)));
    ReportInfo(strform(" azcloudaccountname  [%s]", g_azcloudaccountname));
    ReportInfo(strform(" azresourcegroup     [%s]", g_azresourcegroup));
    ReportInfo(strform(" azsubscription      [%s]", g_azsubscription));
    ReportInfo(strform(" azresourcenetwork   [%s]", g_azresourcenetwork));
    ReportInfo(strform(" poolname            [%s]", g_poolname));
    ReportInfo(strform(" imagename           [%s]", g_imagename));
    ReportInfo(strform(" galleryname         [%s]", g_galleryname));
    ReportInfo(strform(" nodecount           [%d]", g_nodecount));
    ReportInfo(strform(" imageversion        [%s]", g_imageversion));
    ReportInfo(strform(" azjobname           [%s]", g_azjobname));
    ReportInfo(strform(" testscript          [%s]", g_testscript));
    ReportInfo(strform(" timebetweensteps    [%s]", g_timebetweensteps));
    ReportInfo(strform(" stepnamedescriptor  [%s]", g_stepnamedescriptor));
    ReportInfo(strform(" stepnumber          [%d]", g_stepnumber));
    ReportInfo(strform(" totaltime           [%d]", az_gettotaltime()));
    ReportInfo(strform(" useexisting         [%d]", g_useexisting));
    ReportInfo(strform(" project             [%s]", g_project));
    arrdestroy(array);
    l_result = 1;
    
  }
  return l_result;
}

