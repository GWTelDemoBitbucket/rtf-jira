/*
 � Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
   All rights reserved -- Alle Rechte vorbehalten
   GW-TEL� and INFRA-XS� are registered trademarks of Geyer & Weinig GmbH
   GW-TEL� und INFRA-XS� sind eingetragene Warenzeichen der Firma Geyer & Weinig GmbH

 Template for web monitoring with chrome

 Required libraries:
   - AppLib1.gws
   - AppLibWebdriver.gws
   
 This script can be called from batch or from another script, or direct from ApplC.gws
 
 Parameter:
 
 arg1 : Normal arg 1 parameter (<WSName>!x!x)
 
 Parameter only valid when called not from applc.gws:
 arg2 : project, empty string or abnahme, entwicklung
 arg3 : headless, 0 or 1
 arg4 : Rest of parameter with | as separator
        <step name>|<agent>|<timeout>
 arg5 : Last call in this row? (0 or 1)
 arg6 : Do a login every time? (0 or 1)

*/

string g_scriptName;     // Scriptname
string g_service;        // Servicename

// Account
string g_application = "jira";
string g_username = "";
string g_password = "";

// Parameter if any
int g_headless = 0;      // Headless mode 0 or 1
string g_stepname = "";  // Name of the step
string g_agent = "";     // Agent
int g_timeout = 270;
string g_lastrun = "1";   // Is this the last run in this row?
string g_login = "1";     // Do login, if 0 we only do a login if we have no session

// What portal do we use (empty string, abnahme, entwicklung, - is prepended if not empty)
// We set:
// - urls      : If not empty we replace %s with <g_portal> else with empty string
// - g_service : If not empty we set "jira<g_portal>." else only "jira."
string g_portal = "";

// Urls
string g_login_url = "https://projektportal%s.dvag.com/jira/login.jsp";
string g_urls = "https://projektportal%s.dvag.com/jira/secure/Dashboard.jspa?selectPageId=30915," +
                "https://projektportal%s.dvag.com/jira/secure/RapidBoard.jspa?rapidView=844," +
                "https://projektportal%s.dvag.com/jira/secure/RapidBoard.jspa?rapidView=844&projectKey=JIRAPT&view=planning.nodetail&versions=visible&epics=visible&quickFilter=4766," +
                "https://projektportal%s.dvag.com/jira/projects/JIRAPT/summary";

int main() {

  // Check the parameter
  if (!calledFromApplc()) checkParameter();
  
  g_scriptName = "lt-jira.gws";
  g_service    = strform("lt-jira%s.", g_portal);

  if (fileloadscript("AppLib1.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLib1.gws> missing");
    return 1;
  }
  string l_sres = LibInit(arg1);
  if ( l_sres != "" ) {
    ReportError(l_sres);
    return 1;
  }
  if (fileloadscript("AppLibWebdriver.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLibWebdriver.gws> missing");
    return 1;
  }

  // Session handling via rest api
  if (fileloadscript("AppLibSessions.gws") == 0) {
    filelog(3, g_scriptName+": script library <AppLibSessions.gws> missing");
    return 1;
  }

  // Set maximum time for this script
  SysTimeoutSet(g_timeout); // Default is 270 seconds
  
  // Application start, we must close this call with AppStop 
  // where a parameter 0 signals a successful run
  AppStart();
  CounterStart( "Run" );
  CounterParams( g_stepname, g_agent);
  int l_res = initialize();
  if (l_res == 1) {
    l_res = run_tests();
  }
  if (g_lastrun == "1")  finalize();
  if (l_res == 1 ) CounterStop( "Run" );
    else CounterFail( "Run" );
  return AppStop(1-l_res);
}

//
// Initialize browser
//
// Get a session from session manager
// If session is new, we execute a login
//
int initialize() {
  int result = 0;
  int repeat = 1;  // If we have an invalid session, we reconnect
  string error = get_account(g_application);
  if (error == "") {
    if (calledFromApplc()) {
      // Normal call from ApplC.gws
      getSession(g_agent, "chrome", g_headless, calledFromApplc());
      result = doLogin();
    } else {
      // Get session from last test
      while (repeat > 0) {
        if (getSession(g_agent, "chrome", g_headless, 0) == 0) {
          error = getLastWebdriverError();
          if (error == "") {
            // Session was created
            result = doLogin();
          } else {
            ReportError(error);
          }
          repeat = 0;
        } else {
          // Session already exists
          // If we always want a login or the last session is invalid, we delete it and try it again
          if ((g_login == "1") || !checkSession()) {
            removeSession(g_agent, calledFromApplc());
            if (g_login != "1") ReportWarning("Open session was invalid, create a new one");
            // Next loop we create a new session and a new login
            repeat = 1;
          } else {
            result = 1;
            repeat = 0;
          }
        }
      }
    }
  } else {
    ReportError(error);
  }
  return result;
}

//
// Logoff, finalize and close browser
//
void finalize() {
  string counter = "logoff";
  CounterStart(counter);
  CounterParams( g_stepname, g_agent);
  int result = logoff();
  if (result == 1) CounterStop(counter);
    else CounterFail(counter);
  removeSession(g_agent, calledFromApplc());
}

//
// Maeke a login incl conter start/stop
//
int doLogin() {
  string counter = "login";
  CounterStart(counter);
  CounterParams( g_stepname, g_agent);
  string url = strform(g_login_url, g_portal);
  int result = login(url);
  if (result == 1) CounterStop(counter);
    else CounterFail(counter);
  return result;
}

//
// Run all tests
//
int run_tests() {
  int counterlist = ArrFromString("dashboard,kanban,scrum,projects", ",");
  int size = arrgetcount(counterlist);
  // Urls
  int url_array = ArrFromString(g_urls, ",");
  int url_size = arrgetcount(url_array);
  int result = 1;
  int index = 1;
  while ((result == 1) && (index <= size)) {
    string counter = getStringFromArray(counterlist, index);
    string url = strform(getStringFromArray(url_array, index), g_portal);
    CounterStart(counter);
    CounterParams( g_stepname, g_agent);
    if (index == 1) {
      result = dashboard(url);
    } else if (index == 2) {
      result = kanban(url);
    } else if (index == 3) {
      result = scrum(url);
    } else if (index == 4) {
      result = projects(url);
    }
    if (result == 1) CounterStop(counter);
      else CounterFail(counter);
    index = index+1;
  }
  arrdestroy(url_array);
  arrdestroy(counterlist);
  return result;
}

//
// Check if session is OK
//
int checkSession() {
  return check(web_checkElement("xpath", strform("//A[contains(@data-username,'%s')]", g_username, 5000)),
               "Check session failed");
}

//
// Function call for measuring point login
//
int login(string url) {
  return check(web_gotoUrl(url) &&
               web_enterText("id", "login-form-username", g_username, 1) &&
               web_enterText("id", "login-form-password", g_password, 1) &&
               web_click("id", "login-form-submit") &&
               web_checkElement("id", "dvag-datenschutzhinweis-footer"),
               "Error in login");
}
   
//
// Function call for measuring point dashboard
//
int dashboard(string url) {
  return check(web_gotoUrl(url) &&
               web_checkElement("xpath", "//SPAN[text()='Created vs. Resolved Issues']"),
               "Error in dashboard");
}
   
//
// Function call for measuring point kanban
//
int kanban(string url) {
  return check(web_gotoUrl(url) &&
               web_checkElement("xpath", "//H2[text()='Geschlossen']"),
               "Error in kanban");
}
   
//
// Function call for measuring point scrum
//
int scrum(string url) {
  return check(web_gotoUrl(url) &&
               web_checkElement("xpath", "//BUTTON[text()='Vorgang erstellen']"),
               "Error in scrum");
}
   
//
// Function call for measuring point projects
//
int projects(string url) {
  return check(web_gotoUrl(url) &&
               web_checkElement("xpath", "//SPAN[contains(text(),'Aktivit')]"),
               "Error in projects");
}
   
//
// Function call for measuring point logoff
//
int logoff() {
  return check(web_click("css selector", "#user-options > #header-details-user-fullname > .aui-avatar > .aui-avatar-inner > img") &&
               web_click("link text", "Abmelden") &&
               web_checkElement("xpath", "//STRONG[text()='Sie sind jetzt abgemeldet. Jedwede automatische Anmeldung wurde ebenfalls gestoppt.']"),
               "Error in logoff");
}

//////////////////////////////////////////////////////////////////////////////
//
// Some utilities
//
//////////////////////////////////////////////////////////////////////////////

//
// Get the counter name from the array
//
string getStringFromArray(int array, int index) {
  string result = "";
  arrfind(array, asstring(index), &result);
  return result;
}

//
// Checks if the result is ok and shows an error if not
//
// Result is the result param
//

int check(int result, string error) {
  if (result == 0) {
    string lasterror = web_lastError();
    if (lasterror != "") {
      ReportError(strform("%s (%s)", error, lasterror));
    } else {
      ReportError(error);
    }
  }
  return result;
}

//
// Get current user/password
//
string get_account(string application) {
  string appl = strform("%s%s", application, g_portal);
  g_username = "";
  g_password = "";

  // Benutzerkennung aus Zugangsverwaltung einlesen
  if (filegetacct(strform("%s@%s", appl, g_wsname), &g_username, &g_password) == 0) {
    if (filegetacct(strform("%s@%s", appl, "localhost"), &g_username, &g_password) == 0) {
      return strform("Keine Benutzerkennung '%s' definiert", appl);
    }
  }
  return "";
}

//
// Get a parameter value
//
string getParameter(string param, string def) {
  string result = param;
  if (result == "") result = def;
  return result;
}

//
// Get a integer value
//
int intGetParameter(string param, int def) {
  string value = getParameter(param, "");
  int result = (value == "") ? def : strval(value);
  return (result == 0) ? def : result;
}

//
// Check the parameter
//
// arg1 : Normal arg 1 parameter (<WSName>!x!x)
// arg2 : project, empty string or abnahme, entwicklung
// arg3 : headless, 0 or 1
// arg4 : Rest of parameter with | as separator
//        <step name>|<agent>|<timeout>|<logoff>|<login>
// arg5 : logoff (again)
// arg6 : login (again)
//
void checkParameter() {
  g_portal = arg2;
  if (g_portal != "") g_portal = "-" + g_portal;
  g_headless = intGetParameter(arg3, 0);
  string l_param1, l_param2, l_param3, l_param4, l_param5;
  int l_count = strparse(arg4, "|", &l_param1, &l_param2, &l_param3);
  g_stepname  = getParameter(l_param1, "");
  g_agent     = getParameter(l_param2, "");
  g_timeout   = intGetParameter(l_param3, 270);
  g_lastrun   = getParameter(arg5, "1");
  g_login     = getParameter(arg6, "1");
}

//
// Check if we are called from applc.gws
//
// When called from applc only arg1 has a value.
// But we ignore arg2 because later we want to create a way to fill this parameter
//
int calledFromApplc() {
  return (arg3 == "") && (arg4 == "") && (arg5 == "") && (arg6 == "");
}
