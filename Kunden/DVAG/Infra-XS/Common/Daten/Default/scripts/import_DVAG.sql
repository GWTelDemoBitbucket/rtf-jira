// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-2020 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Import von Default Daten

*/
#include "db_create.par"         // Sicherheitshalber Installations-Parameter laden
#include "db_include.sql"        // DBMS-spezifische Makro-Definitionen f�r SQL-Skripte
#include "db_type.sql"           // Ermitteln und definieren des Datenbanktyps und -namens
#include "dm_defines.sql"        // Makrodefinitionen des neuen Datenmodells

// Standort (PostgreSQL needs points as decimal separator)
insert into TC_SIT_SITES (SIT_NAME,SIT_DESCRIPTION,SIT_ADDRESS1,SIT_ADDRESS2,sit_city,SIT_ZIP_CODE,SIT_COUNTRY,SIT_FEDERAL_STATE,SIT_LONGITUDE,SIT_LATITUDE)
values('Ettlingen','Ettlingen','Einsteinstr. 22','','Ettlingen','76275','Germany','Baden-W�ttemberg','8.3735855','48.9409855');
insert into TC_SIT_SITES (SIT_NAME,SIT_DESCRIPTION,SIT_ADDRESS1,SIT_ADDRESS2,sit_city,SIT_ZIP_CODE,SIT_COUNTRY,SIT_FEDERAL_STATE,SIT_LONGITUDE,SIT_LATITUDE)
values('Frankfurt Rechenzentrum','Frankfurt Rechenzentrum','Eschborner Landstra�e 110','','Frankfurt','60489','Germany','Hessen','8.598113','50.129737');

// Relation site person
#define SITE Ettlingen
#include "include_site_person.sql"
#define SITE Frankfurt Rechenzentrum
#include "include_site_person.sql"

// Betriebssystem
insert into TC_OSY_OPERATING_SYSTEMS (OSY_NAME,OSY_VERSION,OSY_BUILD,OSY_RELEASE,OSY_SERVICEPACK)
values('Windows 10 Enterprise','1803','17134','','');
insert into TC_OSY_OPERATING_SYSTEMS (OSY_NAME,OSY_VERSION,OSY_BUILD,OSY_RELEASE,OSY_SERVICEPACK)
values('Windows 7 Enterprise','7.1','7601','','Service Pack 1');
insert into TC_OSY_OPERATING_SYSTEMS (OSY_NAME,OSY_VERSION,OSY_BUILD,OSY_RELEASE,OSY_SERVICEPACK)
values('Windows XP','5.1','2600','','Service Pack 3');

// Verzeichnisse
insert into TC_DIR_DIRECTORIES (DIR_NAME,DIR_DATA,DIR_ROOT,DIR_WINDOWS)
values('Windows XP','C:\Dokumente und Einstellungen\All Users\Anwendungsdaten\Geyer und Weinig\INFRA-XS\default','C:\Programme\Geyer und Weinig\INFRA-XS','C:\Windows');
insert into TC_DIR_DIRECTORIES (DIR_NAME,DIR_DATA,DIR_ROOT,DIR_WINDOWS)
values('WIndows 7','C:\ProgramData\Geyer und Weinig\INFRA-XS\default','C:\Program Files\Geyer und Weinig\INFRA-XS','C:\Windows');
insert into TC_DIR_DIRECTORIES (DIR_NAME,DIR_DATA,DIR_ROOT,DIR_WINDOWS)
values('WIndows 7 X-custom','C:\Infra-xs\ixdata','C:\Infra-xs','C:\Windows');
insert into TC_DIR_DIRECTORIES (DIR_NAME,DIR_DATA,DIR_ROOT,DIR_WINDOWS)
values('Windows 7 (64-Bit)','C:\ProgramData\Geyer und Weinig\INFRA-XS\default','C:\Program Files (x86)\Geyer und Weinig\INFRA-XS','C:\Windows');
insert into TC_DIR_DIRECTORIES (DIR_NAME,DIR_DATA,DIR_ROOT,DIR_WINDOWS)
values('Windows 10 (64-Bit)','C:\ProgramData\Geyer und Weinig\INFRA-XS\default','C:\Program Files (x86)\Geyer und Weinig\INFRA-XS','C:\Windows');


// Scripte
insert into hc_asm_measuring_assemblies (ASM_NAME,ASM_DESCRIPTION,ASM_SCRIPT,ASM_FOLDER,ASM_SERVICE,ASM_INTERVAL)
values('Performance Analyse DVAG','Performance Analyse DVAG','dvagjira.gws','dvag','dvagjira.%',600);
insert into hc_asm_measuring_assemblies (ASM_NAME,ASM_DESCRIPTION,ASM_SCRIPT,ASM_FOLDER,ASM_SERVICE,ASM_INTERVAL)
values('Performance Test','Performance Test','jira.gws','dvag','jira.%',600);
insert into hc_asm_measuring_assemblies (ASM_NAME,ASM_DESCRIPTION,ASM_SCRIPT,ASM_FOLDER,ASM_SERVICE,ASM_INTERVAL)
values('Performance Test Entwicklung','Performance Test Entwicklung','jira.gws','dvagh','jira-hv.%',600);
insert into hc_asm_measuring_assemblies (ASM_NAME,ASM_DESCRIPTION,ASM_SCRIPT,ASM_FOLDER,ASM_SERVICE,ASM_INTERVAL)
values('Performance Test Abnahme','Performance Test Abnahme','jira.gws','dvaga','jira-abnahme.%',600);
insert into hc_asm_measuring_assemblies (ASM_NAME,ASM_DESCRIPTION,ASM_SCRIPT,ASM_FOLDER,ASM_SERVICE,ASM_INTERVAL)
values('Performance Test (headless)','Performance Test (headless)','jiraheadless.gws','dvag','jira-headless.%',600);

// Beziehungtabelle ta_mar_per_asm_om
#define ANWENDUNG Performance Analyse DVAG
#include "import_custom_ta_mar_per_asm_om.sql"
#define ANWENDUNG Performance Test
#include "import_custom_ta_mar_per_asm_om.sql"
#define ANWENDUNG Performance Test Entwicklung
#include "import_custom_ta_mar_per_asm_om.sql"
#define ANWENDUNG Performance Test Abnahme
#include "import_custom_ta_mar_per_asm_om.sql"
#define ANWENDUNG Performance Test (headless)
#include "import_custom_ta_mar_per_asm_om.sql"

// Tabelle Tenant-Script Beziehung ta_rtm_ten_asm_om 
#define ANWENDUNG Performance Analyse DVAG
#include "import_custom_ta_rtm_ten_asm_om.sql"
#define ANWENDUNG Performance Test
#include "import_custom_ta_rtm_ten_asm_om.sql"
#define ANWENDUNG Performance Test Entwicklung
#include "import_custom_ta_rtm_ten_asm_om.sql"
#define ANWENDUNG Performance Test Abnahme
#include "import_custom_ta_rtm_ten_asm_om.sql"
#define ANWENDUNG Performance Test (headless)
#include "import_custom_ta_rtm_ten_asm_om.sql"

// Messpunkte
// Performance Analyse DVAG
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.Run','Gesamtablauf','0','0','1','30.00','60.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.login','Login to Jira','1','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.dashboard','Call Dashboard','2','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.dashboard.openitems','Dashboard - Show open Items','3','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.dashboard.myitems','Dashboard - Show my Items','4','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.dashboard.filter','Dashboard - Set Filter','5','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.dashboard.reset','Dashboard - Reset Filter','6','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban','Call Kanban Board','7','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.selectitem','Kanban - Select Item','8','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.move01','Kanban - Move from Test to Pr�fung','9','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.move02','Kanban - Move from Pr�fung to Umsetzung','10','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.move03','Kanban - Move from Umsetzung to Test ','11','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.filter','Kanban - Set Filter','12','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.kanban.reset','Kanban - Reset Filter','13','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.scrum','Call Scrum Board','14','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.scrum.filter','Scrum - Set Filter','15','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.scrum.reset','Scrum - Reset Filter','16','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.projects','Call Project List','17','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.projects.statistic','Projects - Show Statistic','18','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.projects.activity','Projects - Show activities','19','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search','Search Issue','20','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search.type','Search Issue - Type','21','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search.state','Search Issue - State','22','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search.info','Search Issue - Info','23','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search.date','Search Issue - Date','24','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.search.text','Search Issue - Text','25','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem','Create Issue','26','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.select','Create Issue - Select Type','27','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.create','Create Issue - Create','28','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.edit','Create Issue - Edit Issue','29','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.check','Create Issue - Check','30','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.assign','Create Issue - Assign to me','31','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.comment','Create Issue - Comment','32','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.implement','Create Issue - Implement','33','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.createitem.cancel','Create Issue - Cancel','34','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.booking','Booking','35','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.booking.logtime','Booking - Log time','36','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.booking.delete','Booking - Delete Issue','37','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openrequest','Open Request','38','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openrequest.filter','Open Request - Filter','39','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.opendata','Create Data','40','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.opendata.select','Create Data - Select Type','41','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.opendata.create','Create Data - Create Issue','42','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.opendata.delete','Create Data - Delete Issue','43','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openhelp','Create Helpline','44','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openhelp.select','Create Helpline - Select Type','45','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openhelp.create','Create Helpline - Create Issue','46','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.openhelp.delete','Create Helpline - Delete Issue','47','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'dvagjira.logoff','Logout from Jira','48','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');

// Performance Test
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.Run','Gesamtablauf','0','0','1','30.00','60.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.login','Login to Jira','1','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.dashboard','Call Dashboard','2','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.kanban','Call Kanban Board','7','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.scrum','Call Scrum Board','14','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.scrum.filter','Scrum - Set Filter','15','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.scrum.reset','Scrum - Reset Filter','16','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.projects','Call Project List','17','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira.logoff','Logout from Jira','48','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');

// Performance Test Entwicklung
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.Run','Gesamtablauf','0','0','1','30.00','60.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.login','Login to Jira','1','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.dashboard','Call Dashboard','2','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.kanban','Call Kanban Board','7','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.scrum','Call Scrum Board','14','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.scrum.filter','Scrum - Set Filter','15','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.scrum.reset','Scrum - Reset Filter','16','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.projects','Call Project List','17','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-hv.logoff','Logout from Jira','48','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');

// Performance Test Abnahme
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.Run','Gesamtablauf','0','0','1','30.00','60.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.login','Login to Jira','1','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.dashboard','Call Dashboard','2','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.kanban','Call Kanban Board','7','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.scrum','Call Scrum Board','14','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.scrum.filter','Scrum - Set Filter','15','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.scrum.reset','Scrum - Reset Filter','16','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.projects','Call Project List','17','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-abnahme.logoff','Logout from Jira','48','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');

// Performance Test (headless)
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.Run','Gesamtablauf','0','0','1','30.00','60.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.login','Login to Jira','1','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.dashboard','Call Dashboard','2','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.kanban','Call Kanban Board','7','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.scrum','Call Scrum Board','14','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.scrum.filter','Scrum - Set Filter','15','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.scrum.reset','Scrum - Reset Filter','16','0','0','5.00','10.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.projects','Call Project List','17','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');
insert into hc_pts_measuring_points(pts_name,pts_description,pts_order_no,pts_critical_point,pts_runcounter,pts_runtime_green,pts_runtime_red,pts_performance_green,pts_performance_red,pts_availability_green,pts_availability_red,pts_it_index_green,pts_it_index_red)
values( 'jira-headless.logoff','Logout from Jira','48','1','0','10.00','20.00','98.00','95.00','98.00','95.00','98.00','95.00');


// Beziehungstabelle Messpunkt zu Script
#define ANWENDUNG Performance Analyse DVAG
#define PATTERN dvagjira
#include "import_custom_ta_rmn_asm_pts_om.sql"
#define ANWENDUNG Performance Test
#define PATTERN jira
#include "import_custom_ta_rmn_asm_pts_om.sql"
#define ANWENDUNG Performance Test Entwicklung
#define PATTERN jira-hv
#include "import_custom_ta_rmn_asm_pts_om.sql"
#define ANWENDUNG Performance Test Abnahme
#define PATTERN jira-abnahme
#include "import_custom_ta_rmn_asm_pts_om.sql"
#define ANWENDUNG Performance Test (headless)
#define PATTERN jira-headless
#include "import_custom_ta_rmn_asm_pts_om.sql"

//
// Agenten
//
// Default data for all agents
#define SERVERIP 40.71.45.63
#define AGENTENIP dynamic
#define OS Windows 10 Enterprise
#define SITE Frankfurt Rechenzentrum
// Specific data
#define AGENTENNAME G10AGENT1
#include "import_custom_agent.sql"
#define AGENTENNAME G10AGENT2
#include "import_custom_agent.sql"
#define AGENTENNAME G10AGENT3
#include "import_custom_agent.sql"
#define AGENTENNAME S12AGENT1
#include "import_custom_agent.sql"
#define AGENTENNAME S12AGENT2
#include "import_custom_agent.sql"
#define AGENTENNAME S12AGENT3
#include "import_custom_agent.sql"
#define AGENTENNAME S12AGENT4
#include "import_custom_agent.sql"

// Messauftr�ge
#include "include_measuring.sql"

// Analysis data
#include "include_analysis.sql"



commit;