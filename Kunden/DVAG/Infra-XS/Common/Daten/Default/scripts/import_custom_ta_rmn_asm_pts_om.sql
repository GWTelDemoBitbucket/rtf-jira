// CPP MODE

/*

  $Revision$
  $IsoDate$

  © Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Beziehungstabelle Anwendung -> Messpunkt
  Benutzte Variablen sind
  
    ANWENDUNG  Anwendung
    PATTERN    Pattern zur Berechnung der Messpunkte

*/

insert into ta_rmn_asm_pts_om (rmn_rev_id, rmn_asm_id, rmn_asm_hist_id, rmn_pts_id, rmn_pts_hist_id)
select tc_rev_revisions.rev_id, hc_asm_measuring_assemblies.asm_id, hc_asm_measuring_assemblies.asm_hist_id, hc_pts_measuring_points.pts_id, hc_pts_measuring_points.pts_hist_id
from tc_rev_revisions, hc_asm_measuring_assemblies, hc_pts_measuring_points
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and hc_asm_measuring_assemblies.asm_name = \'ANWENDUNG\'
and hc_pts_measuring_points.pts_name like \'PATTERN.%\';


