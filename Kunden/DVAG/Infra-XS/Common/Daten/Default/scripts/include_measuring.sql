// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-2020 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Create measuring events
  
*/

// Messauftr�ge

#define MESSAUFTRAG Performance Analyse DVAG
#define ALIAS wxdvagjira
#define CRONSTRING 0 0/10 1/1 ? * * *
#define CRONMINUTE 0,10,20,30,40,50
#define TIMEOUT 580
#define KILLLIST chrome,chromedriver
#define AGENTENNAME S12AGENT3
#include "import_custom_messauftrag.sql"

#define MESSAUFTRAG Performance Test
#define ALIAS wxjira
#define CRONSTRING 0 0/10 1/1 ? * * *
#define CRONMINUTE 0,10,20,30,40,50
#define TIMEOUT 280
#define KILLLIST chrome,chromedriver
#define AGENTENNAME S12AGENT4
#include "import_custom_messauftrag.sql"
#define MESSAUFTRAG Performance Test (headless)
#define ALIAS wxjira
#define CRONSTRING 0 5/10 1/1 ? * * *
#define CRONMINUTE 5,15,25,35,45,55
#define TIMEOUT 280
#define KILLLIST chrome,chromedriver
#define AGENTENNAME S12AGENT4
#include "import_custom_messauftrag.sql"


#define MESSAUFTRAG Performance Test
#define ALIAS wxjira
#define CRONSTRING 0 0/10 1/1 ? * * *
#define CRONMINUTE 0,10,20,30,40,50
#define TIMEOUT 280
#define KILLLIST chrome,chromedriver
#define AGENTENNAME G10AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT2
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT3
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT2
#include "import_custom_messauftrag.sql"

#define MESSAUFTRAG Performance Test Entwicklung
#define ALIAS wxjirahv
#define CRONSTRING 0 3/10 1/1 ? * * *
#define CRONMINUTE 3,13,23,33,43,53
#define TIMEOUT 280
#define KILLLIST chrome,chromedriver
#define AGENTENNAME G10AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT2
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT3
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT2
#include "import_custom_messauftrag.sql"

#define MESSAUFTRAG Performance Test Abnahme
#define ALIAS wxjiraabnahme
#define CRONSTRING 0 6/10 1/1 ? * * *
#define CRONMINUTE 6,16,26,36,46,56
#define TIMEOUT 280
#define KILLLIST chrome,chromedriver
#define AGENTENNAME G10AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT2
#include "import_custom_messauftrag.sql"
#define AGENTENNAME G10AGENT3
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT1
#include "import_custom_messauftrag.sql"
#define AGENTENNAME S12AGENT2
#include "import_custom_messauftrag.sql"
