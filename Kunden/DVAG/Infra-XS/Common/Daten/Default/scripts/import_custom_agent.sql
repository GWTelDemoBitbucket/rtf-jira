// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Allgemeine Datei zum Import der QS Daten eines Agenten
  Benutzte Variablen sind
  
    AGENTENNAME  Name of the agent
    AGENTENIP    IP Address of the agent
    SERVERIP     IP Address of the server
    OS           Operating system 
    SITE         Site of the agent

*/

// Agent
insert into hc_agt_agents (AGT_INVENTORY_ID,AGT_COMPUTERNAME,AGT_ADMINSERVER,AGT_BUILDING,AGT_FLOOR,AGT_ROOM,AGT_SOCKET,AGT_DNSNAME,AGT_IPADDRESS,AGT_SHARE_NAME,AGT_CHECKS,AGT_PROFILE,AGT_PURPOSE,AGT_ACTIVE,AGT_TYPE,AGT_TIMEZONE)
values(\'AGENTENNAME\',\'AGENTENNAME\',\'SERVERIP\','','','','',\'AGENTENNAME\',\'AGENTENIP\','','','','','1','0','+02:00');

// Beziehungtabelle ta_rda_dir_agt_om
insert into ta_rda_dir_agt_om (RDA_REV_ID,RDA_DIR_ID,RDA_AGT_ID,RDA_AGT_HIST_ID)
select tc_rev_revisions.rev_id, tc_dir_directories.dir_id ,hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id
from tc_rev_revisions, tc_dir_directories, hc_agt_agents
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and hc_agt_agents.AGT_COMPUTERNAME = \'AGENTENNAME\'
and  tc_dir_directories.DIR_NAME = \'OS\' ;

// Agent/Kunden Zuordnung
insert into ta_rta_ten_agt_om (rta_rev_id, rta_ten_id, rta_ten_hist_id, rta_agt_id, rta_agt_hist_id)
select tc_rev_revisions.rev_id, hc_ten_tenants.ten_id, hc_ten_tenants.ten_hist_id, hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id
from tc_rev_revisions, hc_ten_tenants, hc_agt_agents
where hc_ten_tenants.ten_name = 'Default tenant'
and tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and hc_agt_agents.AGT_COMPUTERNAME = \'AGENTENNAME\';

// Agent OS
insert into ta_roa_osy_agt_om (roa_rev_id, roa_osy_id, roa_agt_id, roa_agt_hist_id)
select tc_rev_revisions.rev_id, tc_osy_operating_systems.osy_id, hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id
from tc_rev_revisions, tc_osy_operating_systems, hc_agt_agents
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and AGT_COMPUTERNAME = \'AGENTENNAME\'
and tc_osy_operating_systems.osy_name = \'OS\';

// Agent SIT
insert into ta_ria_sit_agt_om (ria_rev_id, ria_sit_id, ria_agt_id, ria_agt_hist_id)
select tc_rev_revisions.rev_id, tc_sit_sites.sit_id, hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id
from tc_rev_revisions, tc_sit_sites, hc_agt_agents
where tc_sit_sites.sit_name = \'SITE\'
and tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and agt_computername = \'AGENTENNAME\';

// Agent Status
insert into ta_rua_sta_agt_om (rua_rev_id,rua_sta_id,rua_agt_id,rua_agt_hist_id)
select tc_rev_revisions.rev_id, tc_sta_status.sta_id, hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id
from tc_rev_revisions, tc_sta_status, hc_agt_agents
where tc_sta_status.sta_name = 'Measuring operation'
and tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and agt_computername = \'AGENTENNAME\';

