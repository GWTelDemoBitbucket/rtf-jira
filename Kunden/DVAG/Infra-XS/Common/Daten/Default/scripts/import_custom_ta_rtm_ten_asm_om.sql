// CPP MODE

/*

  $Revision$
  $IsoDate$

  © Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Beziehungstabelle Anwendung -> Kunde
  Benutzte Variablen sind
  
    ANWENDUNG  Anwendung

*/

insert into ta_rtm_ten_asm_om (rtm_rev_id,rtm_ten_id,rtm_ten_hist_id,rtm_asm_id,rtm_asm_hist_id)
select r.rev_id, p.ten_id, p.ten_hist_id, m.asm_id, m.asm_hist_id from tc_rev_revisions r, hc_ten_tenants p, hc_asm_measuring_assemblies m
where r.rev_start in (select max(rev_start) from tc_rev_revisions )
and p.ten_name = 'Default tenant'
and m.asm_name = \'ANWENDUNG\';
