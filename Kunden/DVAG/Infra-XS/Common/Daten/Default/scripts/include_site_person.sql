// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-20019 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL�� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS�� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Relation table Site -> person
  Variables are
  
    SITE  Site

*/
// Ralation table ta_rie_sit_per_mm
insert into ta_rie_sit_per_mm ( rie_rev_id, rie_per_id, rie_sit_id )
select tc_rev_revisions.rev_id, tc_per_responsible_persons.per_id, tc_sit_sites.sit_id from tc_per_responsible_persons, tc_sit_sites, tc_rev_revisions
where tc_rev_revisions.rev_id in (select max(rev_id) from tc_rev_revisions)
and per_last_name = 'Default responsible person'
and sit_name = \'SITE\';
