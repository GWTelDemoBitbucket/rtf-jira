// CPP MODE

/*

  $Revision$
  $IsoDate$

  © Copyright 1998-2019 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS® ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Beziehungstabelle Anwendung -> Verantwortlicher
  Benutzte Variablen sind
  
    ANWENDUNG  Anwendung

*/

insert into ta_mar_per_asm_om ( mar_rev_id ,mar_per_id,mar_asm_id,mar_asm_hist_id)
select tc_rev_revisions.rev_id, tc_per_responsible_persons.per_id, hc_asm_measuring_assemblies.asm_id, hc_asm_measuring_assemblies.asm_hist_id
from tc_rev_revisions, tc_per_responsible_persons, hc_asm_measuring_assemblies
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and tc_per_responsible_persons.per_last_name = 'Default responsible person'
and hc_asm_measuring_assemblies.asm_name = \'ANWENDUNG\';
