// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-2020 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Allgemeine Datei zum Import der QS Daten eines Agenten
  Benutzte Variablen sind
  
    AGENTENNAME  Name des Agenten
    MESSAUFTRAG  Script name
    ALIAS        wxscr32 alias
    CRONSTRING   Cron string, i.e. 0 1/20 * * * ? *
    CRONMINUTE   Cron minute, i.e. 1,21,41
    TIMEOUT      Timeout in ms
    KILLLIST     Kill list, i.e. chrome,chromedriver

*/

// Messauftrag
insert into hc_ser_measurement_series (SER_NAME,SER_DESCRIPTION,SER_ALIAS,SER_PARAMETERS,SER_CRONSTRING,SER_CRON_SEC,SER_CRON_MIN,SER_CRON_HOUR,SER_CRON_DAY,SER_CRON_MON,SER_CRON_DOW,SER_CRON_YEAR,SER_TIMEOUT,SER_KILLLIST,SER_EXTRAS,SER_ENABLED)
values(\'MESSAUFTRAG AGENTENNAME\',\'GWTEST MESSAUFTRAG AGENTENNAME\',\'ALIAS\','-t 4 -f',\'CRONSTRING\','0',\'CRONMINUTE\','*','*','*','?','*',TIMEOUT,\'KILLLIST\','AsUser',1);

// Zuordnung zum Skript
insert into ta_rms_asm_ser_om (rms_rev_id, rms_asm_id, rms_asm_hist_id, rms_ser_id, rms_ser_hist_id)
select tc_rev_revisions.rev_id, hc_asm_measuring_assemblies.asm_id, hc_asm_measuring_assemblies.asm_hist_id, hc_ser_measurement_series.ser_id, hc_ser_measurement_series.ser_hist_id
from tc_rev_revisions, hc_asm_measuring_assemblies, hc_ser_measurement_series
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and hc_asm_measuring_assemblies.asm_name = \'MESSAUFTRAG\'
and hc_ser_measurement_series.ser_name = \'MESSAUFTRAG AGENTENNAME\';

// Agent Messauftrag
insert into ta_ras_agt_ser_om (ras_rev_id,ras_agt_id,ras_agt_hist_id,ras_ser_id,ras_ser_hist_id)
select tc_rev_revisions.rev_id, hc_agt_agents.agt_id, hc_agt_agents.agt_hist_id, hc_ser_measurement_series.ser_id, hc_ser_measurement_series.ser_hist_id
from tc_rev_revisions, hc_agt_agents, hc_ser_measurement_series
where tc_rev_revisions.rev_start in (select max(rev_start) from tc_rev_revisions )
and hc_ser_measurement_series.ser_name = \'MESSAUFTRAG AGENTENNAME\'
and agt_computername = \'AGENTENNAME\';

// Zuordnung zum Messprofile
insert into ta_rps_pro_ser_mm(rps_rev_id,rps_pro_id,rps_pro_hist_id,rps_ser_id,rps_ser_hist_id)
select tc_rev_revisions.rev_id, 
       hc_pro_measurement_profiles.pro_id, hc_pro_measurement_profiles.pro_hist_id,
       hc_ser_measurement_series.ser_id, hc_ser_measurement_series.ser_hist_id
from tc_rev_revisions, hc_pro_measurement_profiles, hc_ser_measurement_series
where hc_pro_measurement_profiles.pro_name = 'default'
and hc_ser_measurement_series.ser_name = \'MESSAUFTRAG AGENTENNAME\';