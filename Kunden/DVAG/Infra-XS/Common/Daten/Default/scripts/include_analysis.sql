// CPP MODE

/*

  $Revision$
  $IsoDate$

  � Copyright 1998-2020 Geyer & Weinig EDV-Unternehmensberatung GmbH
    Alle Rechte vorbehalten
    GW-TEL� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH
    INFRA-XS� ist ein eingetragenes Warenzeichen der Firma Geyer & Weinig GmbH


  Create analysis data
  
*/

// Organisationseinheit anlegen
insert into HC_ADP_ANALYSIS_DEPARTMENTS
  (ADP_NAME,ADP_DESCRIPTION)
values
  ( 'Default', 'Default')
;

// Alle Gesch�ftsf�lle der Messstruktur in Analysestruktur �bernehmen
insert into HC_AAS_ANALYSIS_ASSEMBLIES (AAS_NAME,AAS_DESCRIPTION)
select ASM_NAME, ASM_DESCRIPTION
  from HC_ASM_MEASURING_ASSEMBLIES
  where ASM_NAME not like '%Reboot%'
    and ASM_NAME not like '%Mailwatch%'
    and ASM_NAME not like '%Serverwatch%'
;

// Alle Messpunkte in Analysestruktur �bernehmen (Feld APT_DESCRIPTION f�r sp�tere Zuordnung zu AAS)
insert into HC_APT_ANALYSIS_POINTS (
       APT_NAME, 
       APT_DESCRIPTION, APT_CRITICAL_POINT,
       APT_RUNCOUNTER,
       APT_ORDER_NO, 
       APT_RUNTIME_GREEN, APT_RUNTIME_RED,
       APT_AVAILABILITY_GREEN, APT_AVAILABILITY_RED, 
       APT_PERFORMANCE_GREEN, APT_PERFORMANCE_RED, 
       APT_IT_INDEX_GREEN, APT_IT_INDEX_RED
       )
select SUBSTR( PTS_NAME, STRPOSFULL( PTS_NAME, '.')+1, 100),
       PTS_NAME, 1,
       case STRPOSFULL( PTS_NAME, '.Run') when 0 then 0 else 1 end,
       PTS_ORDER_NO, 
       PTS_RUNTIME_GREEN, PTS_RUNTIME_RED,
       PTS_AVAILABILITY_GREEN, PTS_AVAILABILITY_RED, 
       PTS_PERFORMANCE_GREEN, PTS_PERFORMANCE_RED, 
       PTS_IT_INDEX_GREEN, PTS_IT_INDEX_RED
  from HC_ASM_MEASURING_ASSEMBLIES,
       TA_RMN_ASM_PTS_OM,
       HC_PTS_MEASURING_POINTS
 where ASM_ID = RMN_ASM_ID
   and RMN_PTS_ID = PTS_ID
;

// Alle Aspekte f�r jeden Gesch�ftsfall anlegen (AAS_ID in Aspektbeschreibung zwischenspeichern)
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Detailzeiten', AAS_ID, 'chart/det' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Detailzeiten Gesch�ftsfall', AAS_ID, 'chart/detbar;default' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Ausf�hrungen', AAS_ID, 'table/exe' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Ausf�hrungen mit Ausfalldaten in %', AAS_ID, 'chart/failexepercent;default' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Ausf��rungen mit Ausfalldaten', AAS_ID, 'chart/failexe' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Maximalzeiten PC-Netz-Host', AAS_ID, 'table/max' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'PC-Netz-Host', AAS_ID, 'chart/avg' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Min / Max Laufzeiten', AAS_ID, 'chart/aus;default' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Standortsicht Anwendungen', AAS_ID, 'map/application-agents;default' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Http Performance', AAS_ID, 'chart/http' from HC_AAS_ANALYSIS_ASSEMBLIES;
insert into HC_ANA_ANALYSIS_ASPECTS (ANA_NAME, ANA_DESCRIPTION, ANA_SQL_NAME) 
select 'Tcp Performance', AAS_ID, 'table/tcp' from HC_AAS_ANALYSIS_ASSEMBLIES;

// Zuordnung Gesch�ftsf�lle zu Aspekten (mittels AAS_ID in Aspektbeschreibung)
insert into TA_RA4_AAS_ANA_OM (RA4_ARV_ID,RA4_AAS_ID,RA4_AAS_HIST_ID,RA4_ANA_ID,RA4_ANA_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       AAS_ID, AAS_HIST_ID, ANA_ID, ANA_HIST_ID
  from HC_AAS_ANALYSIS_ASSEMBLIES,
       HC_ANA_ANALYSIS_ASPECTS
 where 
       ANA_DESCRIPTION ~ '^[[:digit:]]+$' and
       ANA_DESCRIPTION = to_char(AAS_ID, 'FM99999999999999999999')
;

// Zuordnung Messpunkte zu Analysepunkten
insert into TA_RA5_PTS_APT_OM (RA5_ARV_ID,RA5_PTS_HIST_ID,RA5_APT_ID,RA5_APT_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       PTS_HIST_ID, APT_ID, APT_HIST_ID
  from HC_PTS_MEASURING_POINTS,
       HC_APT_ANALYSIS_POINTS
 where PTS_NAME = APT_DESCRIPTION
   and PTS_HIST_ID = PTS_ID
   and APT_HIST_ID = APT_ID
;

// Hierarchie f�r alle Analysepunkte von Messpunkten �bernehmen
insert into TA_RA3_APT_APT2_OM (ra3_arv_id,ra3_apt_id,ra3_apt_hist_id,ra3_apt2_id,ra3_apt2_hist_id)
select a.ra5_arv_id, a.ra5_apt_id, a.ra5_apt_hist_id, b.ra5_apt_id, b.ra5_apt_hist_id
  from TA_RPH_PTS_PTS2_OM,
       TA_RA5_PTS_APT_OM a,
       TA_RA5_PTS_APT_OM b
 where RPH_PTS_HIST_ID  = A.RA5_PTS_HIST_ID
   and RPH_PTS2_HIST_ID = B.RA5_PTS_HIST_ID
;

// Zuordnung Analysepunkte zu Gesch�ftsf�llen mittels AAS_NAME in APT_DESCRIPTION
insert into TA_RA6_AAS_APT_OM (RA6_ARV_ID,RA6_AAS_ID,RA6_AAS_HIST_ID,RA6_APT_ID,RA6_APT_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       AAS_ID, AAS_HIST_ID, APT_ID, APT_HIST_ID
  from HC_AAS_ANALYSIS_ASSEMBLIES,
       HC_APT_ANALYSIS_POINTS
 where UPPER( AAS_NAME) = UPPER( SUBSTR( APT_DESCRIPTION, 1, STRPOSFULL( APT_DESCRIPTION, '.')-1))
   and AAS_HIST_ID = AAS_ID
   and APT_HIST_ID = APT_ID
;

// Zuordnung Gesch�ftsf�lle zu Organisationseinheiten
insert into TA_RA1_ADP_AAS_OM (RA1_ARV_ID,RA1_ADP_ID,RA1_ADP_HIST_ID,RA1_AAS_ID,RA1_AAS_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       ADP_ID, ADP_HIST_ID, AAS_ID, AAS_HIST_ID
  from HC_ADP_ANALYSIS_DEPARTMENTS,
       HC_AAS_ANALYSIS_ASSEMBLIES
 where ADP_HIST_ID = ADP_ID
   and AAS_HIST_ID = AAS_ID
;

// Zuordnung Agenten zu Organisationseinheiten
insert into TA_RDG_AGT_ADP_MM (RDG_ARV_ID,RDG_ADP_ID,RDG_ADP_HIST_ID,RDG_AGT_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       ADP_ID, ADP_HIST_ID, AGT_HIST_ID
  from HC_ADP_ANALYSIS_DEPARTMENTS,
       HC_AGT_AGENTS
 where ADP_HIST_ID = ADP_ID
   and AGT_HIST_ID = AGT_ID 
;

// Zuordnung Analysepunkte zu Aspekten
insert into TA_RAP_ANA_APT_MM (RAP_ARV_ID,RAP_ANA_ID,RAP_ANA_HIST_ID,RAP_APT_ID,RAP_APT_HIST_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       ANA_ID, ANA_HIST_ID, APT_ID, APT_HIST_ID
  from HC_ANA_ANALYSIS_ASPECTS,
       HC_APT_ANALYSIS_POINTS,
       HC_AAS_ANALYSIS_ASSEMBLIES,
       TA_RA6_AAS_APT_OM
 where ANA_HIST_ID = ANA_ID
   and APT_HIST_ID = APT_ID 
   and ANA_DESCRIPTION = to_char(AAS_ID, 'FM99999999999999999999')
   and RA6_AAS_ID = AAS_ID
   and RA6_APT_ID = APT_ID
   and ((ANA_SQL_NAME like 'chart/det%' AND APT_RUNCOUNTER != '1')
    or (ANA_SQL_NAME not like 'chart/det%' AND APT_RUNCOUNTER = '1'))
;

//
// Sicht und Sichttypen und Beziehungen erstellen
//

// Unter PostgreSQL liefert bei mehreren Zeilen eine Funktion, z.B. FCT_ARV_CURRENT_ID
// immer den selben Wert zur�ck, wodurch es bei mehreren inserts (z.B. in TA_RA7_AVT_AVW_OM)
// zu einem Fehler kommt (unique constraint)
// Bei den Viewtypes gibt es schon einen Beispiel-Eintrag, den l�schen wir dehalb hier.
// Somit gibt es danach nur noch einen Viewtype.
delete from TC_AVT_ANALYSIS_VIEWTYPES;

// Standard-Typ anlegen
insert into TC_AVT_ANALYSIS_VIEWTYPES
  (AVT_NAME,AVT_DESCRIPTION)
values
  ( 'Default', 'Default');

// Standard-Sicht anlegen
insert into HC_AVW_ANALYSIS_VIEWS
  (AVW_NAME,AVW_DESCRIPTION)
values
  ( 'Default', 'Default');

// Beziehungstabelle Typ zu Sicht
insert into TA_RA7_AVT_AVW_OM (ra7_arv_id,ra7_avw_id,ra7_avw_hist_id,ra7_avt_id)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       avw_id, avw_hist_id, avt_id
  from TC_AVT_ANALYSIS_VIEWTYPES,
       HC_AVW_ANALYSIS_VIEWS
;

// Beziehungstabelle Sicht zu Gruppe
insert into TA_RA8_AVW_ADP_MM (ra8_arv_id,ra8_avw_id,ra8_avw_hist_id,ra8_adp_id)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID, 
       avw_id, avw_hist_id, adp_id
  from hc_adp_analysis_departments, 
       HC_AVW_ANALYSIS_VIEWS
;

// Zuordnung Agenten zu Views
insert into TA_RAG_AGT_AVW_MM (RAG_ARV_ID,RAG_AGT_HIST_ID,RAG_AVW_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       AGT_HIST_ID, AVW_ID
  from HC_AVW_ANALYSIS_VIEWS,
       HC_AGT_AGENTS
 where AVW_HIST_ID = AVW_ID
   and AGT_HIST_ID = AGT_ID
   and AGT_ACTIVE = '1'
   and AGT_ID >= 1000000
;

// Zuordnung Standorte zu Views
insert into TA_RAI_SIT_AVW_MM (RAI_ARV_ID,RAI_SIT_ID,RAI_AVW_ID)
select FCT_ARV_CURRENT_ID(SYSDATE) ARV_ID,
       SIT_ID, AVW_ID
  from HC_AVW_ANALYSIS_VIEWS,
       TC_SIT_SITES
 where AVW_HIST_ID = AVW_ID
   and sit_description != 'Sample';
;


// AAS_ID aus ANA_DESCRIPTION wieder l�schen bzw. durch ANA_NAME ersetzen
DISABLE_TRIGGER(HC_ANA_ANALYSIS_ASPECTS, TRG_ANA_RAP_ARV_BEFORE);
DISABLE_TRIGGER(HC_ANA_ANALYSIS_ASPECTS, TRG_ANA_RA4_ARV_BEFORE);
update HC_ANA_ANALYSIS_ASPECTS set ANA_DESCRIPTION = ANA_NAME;
ENABLE_TRIGGER(HC_ANA_ANALYSIS_ASPECTS, TRG_ANA_RAP_ARV_BEFORE);
ENABLE_TRIGGER(HC_ANA_ANALYSIS_ASPECTS, TRG_ANA_RA4_ARV_BEFORE);
